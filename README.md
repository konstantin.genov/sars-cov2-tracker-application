
Java Spring Boot application built from scratch to track reported data of confirmed Coronavirus infections SARS-CoV2 (COVID-199) around the world.

The data source is provided by the Center for Systems Science and Engineering (CSSE) at Johns Hopkins University.
