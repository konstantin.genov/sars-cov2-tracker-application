package com.kgenov.coroanivurstracker.controllers;
/* konstantin created on 2/20/2021 inside the package - com.kgenov.coroanivurstracker.controllers */

import com.kgenov.coroanivurstracker.models.LocationStats;
import com.kgenov.coroanivurstracker.services.CoronavirusDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

// I will not be using RestControllers as I'll be working with Thymeleaf
@Controller
public class HomeController {

    @Autowired
    CoronavirusDataService virusDataService ;

    @GetMapping("/")
    public String home(Model model) {
        List<LocationStats> allStatistics = virusDataService.getStatistics();
        int totalCasesReported = allStatistics
                .stream()
                .mapToInt(statistic -> statistic.getLatestTotalCasesReported())
                .sum();
        int totalNewCasesReported = allStatistics
                .stream()
                .mapToInt(statistic -> statistic.getDifferenceFromPreviousDay())
                .sum();
        model.addAttribute("locationStatistics", allStatistics);
        model.addAttribute("totalReportedCases", totalCasesReported);
        model.addAttribute("totalNewCasesReported", totalNewCasesReported);
        return "home";
    }
}
