package com.kgenov.coroanivurstracker.services;
/* konstantin created on 2/19/2021 inside the package - com.kgenov.coroanivurstracker.services */

import com.kgenov.coroanivurstracker.models.LocationStats;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class CoronavirusDataService {

    private static final String VIRUS_DATA_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";

    private List<LocationStats> statistics = new ArrayList<>();

    public List<LocationStats> getStatistics() {
        return statistics;
    }

    /**
     * The method will make an HTTP call to an URL in order to fetch the CSV data for globally reported COVID-19 cases.
     * The data source is provided by the Center for Systems Science and Engineering (CSSE) at Johns Hopkins University.
     */
    @PostConstruct // when the instance of the service is constructed, executes the method
    @Scheduled(cron = "* * 1 * * *") // schedules a cron job to periodically make requests and update tracker data
    public void fetchVirusData() throws IOException, InterruptedException {
        StringReader csvBodyReader = new StringReader(requestHTTPResponse()); // captures the http body response
        populateStatisticalData(csvBodyReader); // parses the CSV data from the string, then populates the arrays used for data persistence
    }


    /**
     * Method creates an HTTPClient, then creates a request with the final VIRUS_DATA_URL and finally it saves the HttpResponse
     * @return String of the HTTPResponse body which will be used for CSV operations
     * @throws IOException
     * @throws InterruptedException
     */
    private String requestHTTPResponse() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest dataRequest = HttpRequest.newBuilder().
                uri(URI.create(VIRUS_DATA_URL)).build();
        HttpResponse<String> httpResponse = client.send(dataRequest, HttpResponse.BodyHandlers.ofString());
        return httpResponse.body();
    }

    /**
     * Method iterates through CSV records provided as a StringReader, then parts of those records are stored in a locally initialized ArrayList, finally copied to the main list used for storage.
     * This is done so to avoid concurrency issues while we are parsing the records, as a client might make a request to the list and we'll run into an exception.
     * @param csvBodyReader - StringReader object containing the HTTPResponse body from the requestHTTPResponse() method
     * @throws IOException
     */
    private void populateStatisticalData(StringReader csvBodyReader) throws IOException {
        List<LocationStats> newestStatistics = new ArrayList<>(); // second AL instance to prevent concurrency issues by users requesting our data; the data from this AL will be merged

        Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(csvBodyReader);
        for (CSVRecord record : records) {
            LocationStats locationStatistic = new LocationStats();
            int latestCases = Integer.parseInt(record.get(record.size() - 1));
            int prevDayCases = Integer.parseInt(record.get(record.size() - 2));

            if(record.get("Province/State").isEmpty()){
                locationStatistic.setState("N/A");
            } else {
                locationStatistic.setState(record.get("Province/State"));
            }
            locationStatistic.setCountry(record.get("Country/Region"));
            locationStatistic.setLatestTotalCasesReported(Integer.parseInt(record.get(record.size() - 1)));
            locationStatistic.setDifferenceFromPreviousDay(latestCases - prevDayCases);

            newestStatistics.add(locationStatistic);
        }
        this.statistics = newestStatistics;
    }
}
