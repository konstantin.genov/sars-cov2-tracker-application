package com.kgenov.coroanivurstracker.models;
/* konstantin created on 2/20/2021 inside the package - com.kgenov.coroanivurstracker.models */

public class LocationStats {

    private String state;
    private String country;
    private int latestTotalCasesReported;
    private int differenceFromPreviousDay;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getLatestTotalCasesReported() {
        return latestTotalCasesReported;
    }

    public void setLatestTotalCasesReported(int latestTotalCasesReported) {
        this.latestTotalCasesReported = latestTotalCasesReported;
    }

    public int getDifferenceFromPreviousDay() {
        return differenceFromPreviousDay;
    }

    public void setDifferenceFromPreviousDay(int differenceFromPreviousDay) {
        this.differenceFromPreviousDay = differenceFromPreviousDay;
    }

    @Override
    public String toString() {
        return "LocationStats{" +
                "state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", latestTotalCasesReported=" + latestTotalCasesReported +
                '}';
    }
}
