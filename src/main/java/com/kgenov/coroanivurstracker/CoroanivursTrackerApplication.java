package com.kgenov.coroanivurstracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling // enables scheduled/cron jobs
public class CoroanivursTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoroanivursTrackerApplication.class, args);
	}

}
